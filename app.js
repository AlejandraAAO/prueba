const app = new Vue({
    el:'#app',
    data:{
        message:'un mensaje largo',
        counter:0
    },
    computed:{
        //es como pasar datos q van a tener logica
        //traer los datos de data nos dis
        inverted(){
            return this.message.split('').reverse().join('');
        },
        color() {
            return {
                //condiciono el stylo con las variable computadas

                'bg-success': this.counter < 10,
                'bg-warning': this.counter >= 10 && this.counter < 20,
                'bg-danger' : this.counter >= 20
            }
        }
    }
})